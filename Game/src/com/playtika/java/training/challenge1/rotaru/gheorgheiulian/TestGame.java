package com.playtika.java.training.challenge1.rotaru.gheorgheiulian;

import com.playtika.java.training.challenge1.rotaru.gheorgheiulian.exceptions.PlayerProfileDataException;
import com.playtika.java.training.challenge1.rotaru.gheorgheiulian.player.PlayerProfile;

import java.sql.SQLOutput;

public class TestGame {

    public static void main(String[] args) {
        PlayerProfile player1 = null;
        PlayerProfile player2 = null;
        try {
            player1 = new PlayerProfile("Johnny", "john@baiat.bun");
            player2 = new PlayerProfile("Frank", "franK99@gmail.com");
        }
        catch (PlayerProfileDataException exception){
            System.out.println(exception.getMessage());
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        System.out.println(player1.toString());
        System.out.println(player2.toString());

        System.out.println();

        for(int i = 1; i < 5; i++) {
            player1.addNewPlaySession();
            System.out.println(player1.toString());
        }

        System.out.println("Before");
        System.out.println(player1.toString());
        System.out.println(player2.toString());
        System.out.println();

        System.out.println("After");
        player2 = player1.clone();
        player2.addNewPlaySession();
        player2.addNewPlaySession();

        System.out.println(player1.toString());
        System.out.println(player2.toString());

        try {
            player1.setEmail("john@baiat.bun");
        }
        catch (PlayerProfileDataException exception){
            System.out.println(exception.getMessage());
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        player1.updateLastPlayedTime(15);
        System.out.println(player1.getUserName());
        System.out.println(player1.getEmail());
        System.out.println(player1.getPlayerAgeInDays());

        try {
            System.out.println(player1.getTotalPlayedTime());
        }
        catch (PlayerProfileDataException exception){
            System.out.println(exception.getMessage());
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        System.out.println(player1.getMaxPlayedMinutes());

    }
}

