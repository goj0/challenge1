package com.playtika.java.training.challenge1.rotaru.gheorgheiulian.exceptions;

public class PlayerProfileDataException extends Exception {
    public PlayerProfileDataException() {

    }

    public PlayerProfileDataException(String mesage) {
        super(mesage);
    }
}
