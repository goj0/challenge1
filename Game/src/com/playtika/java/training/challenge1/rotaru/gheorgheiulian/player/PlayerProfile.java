package com.playtika.java.training.challenge1.rotaru.gheorgheiulian.player;

import com.playtika.java.training.challenge1.rotaru.gheorgheiulian.exceptions.PlayerProfileDataException;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlayerProfile implements Cloneable {
    private final String userName;
    private String email;
    private LocalDateTime creationDate;
    private int noPlayedSessions;
    private int[] minutesPlayedPerSession;

    private static int MAX_PLAYED_MINUTES;

    public PlayerProfile(String userName, String email) throws PlayerProfileDataException {
        this.userName = userName;
        this.setEmail(email);
        this.creationDate = LocalDateTime.now();
        this.noPlayedSessions = 0;
        this.minutesPlayedPerSession = null;

        MAX_PLAYED_MINUTES = 0;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) throws PlayerProfileDataException {

        if(email == null) {
            throw new PlayerProfileDataException("The email is null");
        }

        if(email.isEmpty()) {
            throw new PlayerProfileDataException("The email is empty");
        }

        String matchingUsername = "^([a-zA-Z0-9_.-]{1,})([@])([a-zA-Z0-9.-]{1,})([.])([a-zA-Z0-9_]{2,})$";
        Pattern patternUsername = Pattern.compile(matchingUsername);

        Matcher matcherUsername = patternUsername.matcher(email);

        if(!matcherUsername.matches()) {
            throw new PlayerProfileDataException("The given email is not correct");
        }

        this.email = email;
    }

    public int getTotalPlayedTime() throws PlayerProfileDataException {
        if(noPlayedSessions == 0) {
            throw new PlayerProfileDataException("The minutesPlayedPerSession is null");
        }

        int totalPlayedTime = 0;
        for (int minutesPlayedThisSession : minutesPlayedPerSession) {
            totalPlayedTime += minutesPlayedThisSession;
        }

        return totalPlayedTime;
    }

    public int getPlayerAgeInDays() {
        LocalDateTime now = LocalDateTime.now();
        int days = (int) creationDate.until(now, ChronoUnit.DAYS);

        return days;
    }

    public int getMaxPlayedMinutes() {
        return MAX_PLAYED_MINUTES;
    }

    public void addNewPlaySession() {
        noPlayedSessions++;

        int[] newMinutesPlayedPerSession = new int[noPlayedSessions];

        for(int i = 0; i < noPlayedSessions - 1; i++) {
            newMinutesPlayedPerSession[i] = minutesPlayedPerSession[i];
        }

        newMinutesPlayedPerSession[noPlayedSessions - 1] = 0;
        minutesPlayedPerSession = newMinutesPlayedPerSession;
    }

    public void updateLastPlayedTime(int playedMinutes) {
        MAX_PLAYED_MINUTES = (MAX_PLAYED_MINUTES < playedMinutes) ? playedMinutes : MAX_PLAYED_MINUTES;
        minutesPlayedPerSession[noPlayedSessions - 1] = playedMinutes;
    }

    @Override
    public String toString() {
        return "PlayerProfile{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", creationDate=" + creationDate +
                ", noPlayedSessions=" + noPlayedSessions +
                ", minutesPlayedPerSession=" + Arrays.toString(minutesPlayedPerSession) +
                '}';
    }

    @Override
    public PlayerProfile clone() {
        PlayerProfile newPlayerProfile = null;
        try {
            newPlayerProfile = new PlayerProfile(this.getUserName(), this.getEmail());
        }
        catch (PlayerProfileDataException exception){
            System.out.println(exception.getMessage());
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        newPlayerProfile.creationDate = this.creationDate;
        newPlayerProfile.noPlayedSessions = this.noPlayedSessions;
        newPlayerProfile.minutesPlayedPerSession = new int[this.minutesPlayedPerSession.length];

        for(int i = 0; i < minutesPlayedPerSession.length; i++) {
            newPlayerProfile.minutesPlayedPerSession[i] = minutesPlayedPerSession[i];
        }

        return newPlayerProfile;
    }
}
