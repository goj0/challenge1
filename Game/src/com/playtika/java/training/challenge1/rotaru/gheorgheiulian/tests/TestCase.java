package com.playtika.java.training.challenge1.rotaru.gheorgheiulian.tests;

import com.playtika.java.training.challenge1.rotaru.gheorgheiulian.exceptions.PlayerProfileDataException;
import com.playtika.java.training.challenge1.rotaru.gheorgheiulian.player.PlayerProfile;


import com.playtika.java.training.challenge1.rotaru.gheorgheiulian.tests.suites.TestSelectedCases;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.*;

public class TestCase {

    @Category(TestSelectedCases.class)
    @Test
    public void functionTestWithNoValuesGetTotalPlayedTime() {
        PlayerProfile player = null;

        try {
            player = new PlayerProfile("Frank", "franK99@gmail.com");
        }
        catch (PlayerProfileDataException exception){
            System.out.println(exception.getMessage());
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        String mesage = "nothing";
        try {
            System.out.println(player.getTotalPlayedTime());
        }
        catch (PlayerProfileDataException exception) {
            mesage = exception.getMessage();
        }
        catch (Exception exception) {
            mesage = exception.getMessage();
        }

        assertEquals(mesage, "The minutesPlayedPerSession is null");
    }

    @Category(TestSelectedCases.class)
    @Test
    public void functionTestWithValuesGetTotalPlayedTime() {

        PlayerProfile player = null;

        try {
            player = new PlayerProfile("Frank", "franK99@gmail.com");
        }
        catch (PlayerProfileDataException exception){
            System.out.println(exception.getMessage());
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        for(int i = 1; i < 5; i++) {
            player.addNewPlaySession();
            player.updateLastPlayedTime(i);
        }

        int totalTimePlayed = 0;
        try {
            totalTimePlayed = player.getTotalPlayedTime();
        }
        catch (PlayerProfileDataException exception) {
            System.out.println(exception.getMessage());
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        assertEquals(totalTimePlayed, 10);
    }

    @Category(TestSelectedCases.class)
    @Test
    public void functionTestSetEmailException() {

        PlayerProfile player = null;

        String mesage = "nothing";

        try {
            player = new PlayerProfile("Frank", "franK99gmail.com");
        }
        catch (PlayerProfileDataException exception){
            mesage = exception.getMessage();
        }

        assertNotEquals(mesage, "nothing");
    }

    @Category(TestSelectedCases.class)
    @Test
    public void functionTestGoodSetEmail() {

        PlayerProfile player = null;

        String mesage = "nothing";

        try {
            player = new PlayerProfile("Frank", "franK99@gmail.com");
        }
        catch (PlayerProfileDataException exception){
            mesage = exception.getMessage();
        }

        try {
            player.setEmail("franK9999@gmail.com");
        }
        catch (PlayerProfileDataException exception){
            mesage = exception.getMessage();
        }

        assertEquals(player.getEmail(), "franK9999@gmail.com");
    }

}
