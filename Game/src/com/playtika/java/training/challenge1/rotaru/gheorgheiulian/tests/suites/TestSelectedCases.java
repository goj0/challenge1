package com.playtika.java.training.challenge1.rotaru.gheorgheiulian.tests.suites;

import com.playtika.java.training.challenge1.rotaru.gheorgheiulian.tests.TestCase;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(TestCase.class)
@Categories.IncludeCategory(TestSelectedCases.class)

public class TestSelectedCases {
}
