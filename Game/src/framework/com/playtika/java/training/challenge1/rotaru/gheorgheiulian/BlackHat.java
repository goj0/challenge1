package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian;

import framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.enumerations.CharacterType;

public class BlackHat extends SuperHero {
    public int spellPower;

    public BlackHat(int spellPower) {
        super(30, "Franky", CharacterType.Sorcerer);
        this.spellPower = spellPower;
    }

    public void useMagic() {
        this.spellPower++;
    }

    @Override
    public String toString() {
        return "BlackHat{" +
                "spellPower=" + spellPower +
                '}';
    }
}
