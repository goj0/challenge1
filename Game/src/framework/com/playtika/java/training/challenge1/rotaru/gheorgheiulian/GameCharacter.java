package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian;

import framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.enumerations.CharacterType;
import framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.interfaces.Playable;

public abstract class GameCharacter implements Playable {
    private int lifePoints;
    private String name;
    private CharacterType type;

    public GameCharacter(int lifePoints, String name, CharacterType type) {
        this.lifePoints = lifePoints;
        this.name = name;
        this.type = type;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public String getName() {
        return name;
    }

    @Override
    public int takesAHit(int a) {
        return this.lifePoints - a;
    }

    @Override
    public int heals(int a) {
        return this.lifePoints + a;
    }

    @Override
    public void move() {
        if (type.getSpecialAction().equals("Goes Last")) {
            this.lifePoints = 1;
        }
        else if (type.getSpecialAction().equals("Cast a spell")) {
            this.lifePoints++;
        }
    }

    @Override
    public String toString() {
        return "GameCharacter{" +
                "lifePoints=" + lifePoints +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
