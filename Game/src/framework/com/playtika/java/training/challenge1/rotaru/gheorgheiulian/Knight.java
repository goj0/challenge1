package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian;

import framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.enumerations.CharacterType;

public class Knight extends SuperHero {
    public String ability;
    public int shieldStrength;

    public Knight(String ability, int shieldStrength) {
        super(30, "Arthur The Brave", CharacterType.Paladin);
        this.ability = ability;
        this.shieldStrength = shieldStrength;
    }

    public void useSword() {
        this.ability = "Use Excalibur";
    }

    @Override
    public String toString() {
        return "Knight{" +
                "ability='" + ability + '\'' +
                ", shieldStrength=" + shieldStrength +
                '}';
    }
}
