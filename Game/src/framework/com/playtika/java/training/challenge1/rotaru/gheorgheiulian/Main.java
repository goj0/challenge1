package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian;

public class Main {

    public static void main(String[] args) {
        GameCharacter knight = new Knight("Good sword", 10);
        System.out.println(knight.toString());
        System.out.println(knight.getLifePoints());
        System.out.println(knight.getName());
        knight.move();
        knight.takesAHit(10);
        knight.heals(6);
        System.out.println(knight.toString());

        System.out.println();
        Knight king = new Knight("Good sword", 10);
        System.out.println(king.toString());

        king.useSword();
        System.out.println(king.toString());


        System.out.println();
        BlackHat blackHat = new BlackHat(6);
        System.out.println(blackHat.toString());

        blackHat.useMagic();
        System.out.println(blackHat.toString());
    }
}
