package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian;

import framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.enumerations.CharacterType;
import framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.interfaces.Usable;

public class SuperHero extends GameCharacter implements Usable {
    private boolean isVillain;
    private int lifePoints;
    private String name;
    private CharacterType type;


    public SuperHero(int lifePoints, String name, CharacterType type) {
        super(lifePoints, name, type);
        this.lifePoints = lifePoints;
        this.name = name;
        this.type = type;

        if(name.equals("Arthur The Brave")) {
            this.isVillain = true;
        }
        else {
            this.isVillain = false;
        }
    }

    public boolean isVillain() {
        return isVillain;
    }

    public void breakingBad() {
        this.isVillain = true;
    }

    public void turnGood() {
        this.isVillain = false;
    }

    public void doAction() {
        action();
    }

    @Override
    public String action() {
        String wantedString;

        if(name.equals("Arthur The Brave")) {
            wantedString = "Go to war";
        }
        else {
            wantedString = "Cast at least one spell";
        }

        return  wantedString;
    }

    @Override
    public String toString() {
        return "SuperHero{" +
                "isVillain=" + isVillain +
                ", lifePoints=" + lifePoints +
                ", name='" + name + '\'' +
                '}';
    }
}
