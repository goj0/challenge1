package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.enumerations;

public enum CharacterType {
    Healer("Stay behind"), Paladin("Goes Last"), Sorcerer("Cast a spell"), Gnome("Goes first");

    private String specialAbility;

    private CharacterType(String specialAbility) {
        this.specialAbility = specialAbility;
    }

    public String getSpecialAction() {
        return specialAbility;
    }
}
