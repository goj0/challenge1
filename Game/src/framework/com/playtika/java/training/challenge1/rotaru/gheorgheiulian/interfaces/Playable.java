package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.interfaces;

public interface Playable {
    public final static int MAX_POINTS = 50;

    public int takesAHit(int a);
    public int heals(int a);
    public void move();
}
