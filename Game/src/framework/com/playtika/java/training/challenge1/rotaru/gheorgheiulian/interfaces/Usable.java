package framework.com.playtika.java.training.challenge1.rotaru.gheorgheiulian.interfaces;

public interface Usable {

    public String action();
}
